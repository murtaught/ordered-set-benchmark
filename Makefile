SRC = main.cpp catch.cpp
OBJ = $(SRC:.cpp=.o)
CXXFLAGS = --std=c++20 -O2
LDFLAGS = -O1

all: options main

options:
	@echo build options:
	@echo "CXXFLAGS  = $(CXXFLAGS)"
	@echo "LDFLAGS = $(LDFLAGS)"
	@echo "CXX = $(CXX)"
	@echo ""

.c.o:
	$(CXX) $(CXXFLAGS) -c $<

catch.o: catch.hpp

main: $(OBJ)
	$(CXX) -o $@ $(OBJ) $(LDFLAGS)

clean:
	rm -f main $(OBJ)

.PHONY: all options clean dist install uninstall
