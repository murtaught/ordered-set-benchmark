#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include "catch.hpp"
#include <iostream>
#include <random>
#include <set>
#include <climits>

#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
namespace gnu = __gnu_pbds;

template <typename T>
using ordered_set = gnu::tree<
    T,
    gnu::null_type,
    std::less<T>,
    gnu::rb_tree_tag,
    gnu::tree_order_statistics_node_update
>;

struct SetRange
{
    int l, r;

    explicit SetRange(int l, int r)
        : l(l), r(r)
    {}
};

bool operator<(int i, const SetRange &r) {
    return i < r.l;
}

bool operator<(const SetRange &r, int i) {
    return r.r < i;
}

TEST_CASE("Benchmarks") {
    constexpr int N = 1e5;
    constexpr int Q = 10;

    std::set<int, std::less<>> set;
    ordered_set<int> ord_set;

    std::random_device device;
    std::mt19937 gen(device());
    std::uniform_int_distribution<int> dist{INT_MIN, INT_MAX};
    for (int i = 0; i < N; ++i) {
        const int a = dist(gen);
        set.insert(a);
        ord_set.insert(a);
    }

    std::vector<std::pair<int, int>> queries(Q);
    for (int i = 0; i < Q; ++i) {
        int a = dist(gen), b = dist(gen);
        if (a > b) {
            std::swap(a, b);
        }

        queries[i] = { a, b };
    }

    BENCHMARK("ordered_set") {
        int sum = 0;
        for (const auto &query : queries) {
            sum += ord_set.order_of_key(query.second + 1);
            sum -= ord_set.order_of_key(query.first);
        }
        return sum;
    };

    BENCHMARK("std::distance") {
        int sum = 0;
        for (const auto &query : queries) {
            const auto it_l = set.lower_bound(query.first);
            const auto it_r = set.upper_bound(query.second);
            sum += std::distance(it_l, it_r);
        }
        return sum;
    };

    BENCHMARK("SetRange") {
        int sum = 0;
        for (const auto &query : queries) {
            sum += set.count(SetRange{query.first, query.second});
        }
        return sum;
    };
}

// int main() {
//     set<int, std::less<>> s = { 1, 2, 3, 4, 8, 10, 11, 12 };
//
//     cout << s.count(1) << endl; // 1
//     cout << s.count(5) << endl; // 0
//     cout << s.count(SetRange{2, 2}) << endl; // 1
//     cout << s.count(SetRange{1, 3}) << endl; // 3
//     cout << s.count(SetRange{5, 7}) << endl; // 0
//     cout << s.count(SetRange{2, 12}) << endl; // 7
//
//     return 0;
// }
